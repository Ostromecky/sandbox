FROM node:12.16.3-alpine

WORKDIR /usr/src/app

COPY dist/apps/web-ui dist

#COPY package.json .
#
#RUN npm install --only=prod

EXPOSE 4000

CMD ["node", "dist/server/main.js"]
