module.exports = {
  projects: [
    '<rootDir>/apps/web-ui',
    '<rootDir>/apps/api',
    '<rootDir>/libs/types',
  ],
};
