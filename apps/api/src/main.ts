/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

import { NestFactory } from "@nestjs/core";

import { AppModule } from "./app/app.module";
import * as cookieParser from "cookie-parser";
import { Logger } from "@nestjs/common";
import { GlobalExceptionFilter } from "./app/filters/global-exception.filter";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const globalPrefix = 'api';
  // TODO - remove prefix
  app.setGlobalPrefix(globalPrefix);
  app.use(cookieParser());
  app.enableCors();
  app.useGlobalFilters(new GlobalExceptionFilter());
  const port = Number(process.env.PORT) || 3000;
  await app.listen(port, () => {
    Logger.log('Listening at http://localhost:' + port + '/' + globalPrefix);
  });
}

bootstrap();
