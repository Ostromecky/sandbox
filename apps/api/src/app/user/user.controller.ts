import {
  Body,
  Controller,
  Delete,
  Get,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { plainToClass } from 'class-transformer';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { PageDto } from '../shared/dto/page.dto';
import { CreateUserDto } from './dto/create-user.dto';
import { UserQueryDto } from './dto/user-query.dto';
import { UserDto } from './dto/user.dto';
import { User } from './entity/user.entity';
import { UsersService } from './user.service';

@ApiTags('users')
// @ApiBearerAuth()
@Controller('users')
export class UserController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  async createUser(@Body() createUserDto: CreateUserDto): Promise<User> {
    return await this.usersService.createUser(createUserDto);
  }

  @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: 'Fetch users using query' })
  @Get()
  async findByQuery(
    @Query() queryDto: UserQueryDto,
  ): Promise<PageDto<UserDto>> {
    const query = plainToClass(UserQueryDto, queryDto);
    return await this.usersService.findByQuery(query);
  }

  @UseGuards(JwtAuthGuard)
  @Delete()
  async deleteUser(@Body() userDto: UserDto) {
    return await this.usersService.remove(userDto.id);
  }
}
