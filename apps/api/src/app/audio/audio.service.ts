import { HttpService, Inject, Injectable } from "@nestjs/common";
import { S3 } from "aws-sdk";
import { ConfigService } from "@nestjs/config";
import { ManagedUpload } from "aws-sdk/lib/s3/managed_upload";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { Audio } from "./entities/audio.entity";
import { IAudio } from "./types/audio";
import { S3_PROVIDER } from "../storage";
import { IGetAudio } from "./dto/get-audio.dto";
import SendData = ManagedUpload.SendData;
import { PlaylistService } from "../playlist/playlist.service";
import { ModuleRef } from "@nestjs/core";
import { TransactionFor } from "nest-transact";

@Injectable()
export class AudioService extends TransactionFor<AudioService> {
  constructor(
    private http: HttpService,
    @Inject(S3_PROVIDER) private s3: S3,
    @InjectRepository(Audio)
    private audioRepository: Repository<Audio>,
    private configService: ConfigService,
    private playlistService: PlaylistService,
    moduleRef: ModuleRef
  ) {
    super(moduleRef);
  }

  async upload(file, playlistId: number): Promise<void> {
    const { originalname } = file;
    const bucketS3 = this.configService.get<string>('s3.storageBucketName');
    const uploadedFile = await this.uploadS3(
      file.buffer,
      bucketS3,
      originalname,
    );
    const audio = await this.createAudio(uploadedFile);
    await this.playlistService.addAudioToPlaylist(playlistId, audio);
  }

  async findAll(): Promise<IGetAudio[]> {
    const audioEntities = await this.audioRepository.find();
    return audioEntities.map<IGetAudio>((audio) => ({
      url: audio.location,
      name: audio.key,
      artist: '',
    }));
  }

  remove(id: number) {
    return `This action removes a #${id} audio`;
  }

  private async uploadS3(file, bucket, name): Promise<SendData> {
    const s3 = this.s3;
    const params = {
      Bucket: bucket,
      Key: String(name),
      Body: file,
      ContentType: 'audio/mpeg',
    };

    return new Promise((resolve, reject) => {
      s3.upload(params, (err, data) => {
        if (err) {
          // Logger.error(err);
          console.error(err);
          reject(err.message);
        }
        resolve(data);
      });
    });
  }

  private async createAudio(file: SendData): Promise<Audio> {
    const newFile: IAudio = {
      createdAt: new Date(),
      key: file.Key,
      eTag: file.ETag,
      bucket: file.Bucket,
      location: file.Location,
    };
    const fileEntity = this.audioRepository.create(newFile);
    return await this.audioRepository.save(fileEntity);
  }
}
