export interface IGetAudio {
  url: string;
  name: string;
  artist: string;
  imgUrl?: string;
}
