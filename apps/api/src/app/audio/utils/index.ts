import { Audio } from '../entities/audio.entity';
import { IGetAudio } from '../dto/get-audio.dto';

export const toGetAudioDto = <T extends Audio>(audio: T): IGetAudio => {
  const { key, location, bucket, createdAt, eTag } = audio;
  return {
    name: key,
    url: location,
    // TODO - artist
    artist: '',
  };
};
