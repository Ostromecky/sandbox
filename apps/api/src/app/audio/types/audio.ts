export interface IAudio {
  id?: number;
  createdAt: Date;
  bucket: string;
  key: string;
  location: string;
  eTag: string;
}

export type IAudioLocation = Pick<IAudio, "bucket" | "key">;
