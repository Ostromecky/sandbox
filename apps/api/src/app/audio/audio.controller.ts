import { Controller, Delete, Get, Param, Post, UploadedFile, UseInterceptors } from "@nestjs/common";
import { AudioService } from "./audio.service";
import { FileInterceptor } from "@nestjs/platform-express";

@Controller("audio")
export class AudioController {
  constructor(
    private readonly audioService: AudioService
  ) {
  }

  @Post()
  @UseInterceptors(FileInterceptor("file"))
  uploadFile(@UploadedFile() file, @Param("playlistId") playlistId: number) {
    return this.audioService.upload(file, playlistId);
  }

  @Get()
  findAll() {
    return this.audioService.findAll();
  }

  @Delete(":id")
  remove(@Param("id") id: string) {
    return this.audioService.remove(+id);
  }
}
