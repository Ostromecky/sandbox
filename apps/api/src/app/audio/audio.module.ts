import { HttpModule, Module } from "@nestjs/common";
import { AudioService } from "./audio.service";
import { AudioController } from "./audio.controller";
import { StorageModule } from "../storage/storage.module";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Audio } from "./entities/audio.entity";
import { PlaylistModule } from "../playlist/playlist.module";

@Module({
  controllers: [AudioController],
  providers: [AudioService],
  imports: [
    StorageModule,
    AudioModule,
    HttpModule,
    TypeOrmModule.forFeature([Audio]),
    PlaylistModule
  ],
})
export class AudioModule {}
