import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { IAudio } from '../types/audio';

@Entity()
export class Audio implements IAudio {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn({ name: 'created_at', type: 'timestamptz' })
  createdAt: Date;

  @Column()
  bucket: string;

  @Column()
  key: string;

  @Column()
  location: string;

  @Column()
  eTag: string;
}
