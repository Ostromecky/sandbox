import { ForbiddenException, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcryptjs from 'bcryptjs';
import { User } from '../user/entity/user.entity';
import { UsersService } from '../user/user.service';
import { CreateSessionDto } from './dto/create-session.dto';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async validateUser(dto: CreateSessionDto): Promise<User> {
    const user = await this.usersService.findForAuth(dto.userName);
    if (user) {
      const valid = await bcryptjs.compare(dto.password, user.password);
      if (valid) {
        // delete user.password;
        return user;
      }
    }
    throw new ForbiddenException();
  }

  async login(user: User) {
    const payload = { userName: user.userName, id: user.id };
    return this.jwtService.sign(payload);
  }
}
