import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { CreateSession } from '@sandbox/types';

export class CreateSessionDto extends CreateSession {
  @ApiProperty()
  @IsString()
  readonly userName: string;
  @ApiProperty()
  @IsString()
  readonly password: string;

  constructor(args: CreateSession) {
    super(args);
  }
}
