import { Injectable, ExecutionContext } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Observable } from 'rxjs';
import { ConfigService } from "@nestjs/config";

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {

  constructor(props, private configService: ConfigService) {
    super(props);
  }

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const authEnabled = this.configService.get('jwt.authEnabled');
    if (!authEnabled ) return true;
    return super.canActivate(context);
  }
}
