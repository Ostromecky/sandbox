import { Body, Controller, Post, Res } from '@nestjs/common';
import { AuthService } from './auth.service';
import { CreateSessionDto } from './dto/create-session.dto';
import { ApiTags } from '@nestjs/swagger';
import { Cookies } from '../utils/cookies';

@ApiTags('auth')
@Controller('/auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post()
  async authenticate(
    @Cookies('token') token1: string,
    @Body() createSessionDTO: CreateSessionDto,
    @Res({ passthrough: true }) response,
  ) {
    const validUser = await this.authService.validateUser(createSessionDTO);
    const token = await this.authService.login(validUser);
    response.cookie('token', token, { httpOnly: true });
  }
}
