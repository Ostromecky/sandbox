import { Playlist } from "../entities/playlist.entity";
import { IGetPlaylist } from "../dto/get-playlist.dto";
import { toGetAudioDto } from "../../audio/utils";

export const toGetPlaylistDto = (playlistEntity: Playlist): IGetPlaylist => {
  const { name, audios } = playlistEntity;

  return {
    name,
    audios: [...audios.map((audio) => toGetAudioDto(audio))],
  };
}
