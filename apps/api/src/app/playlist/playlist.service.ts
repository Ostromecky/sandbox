import { Injectable } from '@nestjs/common';
import { CreatePlaylistDto } from './dto/create-playlist.dto';
import { UpdatePlaylistDto } from './dto/update-playlist.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Playlist } from './entities/playlist.entity';
import { Repository } from 'typeorm';
import { IGetPlaylists } from './dto/get-playlists.dto';
import { IGetPlaylist } from './dto/get-playlist.dto';
import { toGetPlaylistDto } from './utils';
import { Audio } from "../audio/entities/audio.entity";

@Injectable()
export class PlaylistService {
  constructor(
    @InjectRepository(Playlist)
    private playlistRepository: Repository<Playlist>
  ) {}

  async create(createPlaylistDto: CreatePlaylistDto): Promise<void> {
    const playlist: Playlist = this.playlistRepository.create(
      createPlaylistDto,
    );
    await this.playlistRepository.save(playlist);
  }

  async addAudioToPlaylist(id: number, audio: Audio): Promise<void> {
    const playlist: Playlist = await this.playlistRepository.findOne({ id });
    playlist.audios.push(audio);
    await this.playlistRepository.save(playlist);
  }

  async findAll(): Promise<IGetPlaylists[]> {
    const playlists = await this.playlistRepository.find();
    return playlists.map<IGetPlaylists>((playlist) => ({
      name: playlist.name,
    }));
  }

  async findOne(id: number): Promise<IGetPlaylist> {
    const playlist = await this.playlistRepository.findOne({
      where: { id },
      relations: ['audios'],
    });
    return toGetPlaylistDto(playlist);
  }

  async update(id: number, updatePlaylistDto: UpdatePlaylistDto) {
    return await this.playlistRepository.update(id, updatePlaylistDto);
  }

  remove(id: number) {
    return `This action removes a #${id} playlist`;
  }
}
