import { Column, CreateDateColumn, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn } from "typeorm";
import { Audio } from "../../audio/entities/audio.entity";

@Entity()
export class Playlist {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn({ name: "created_at", type: "timestamptz" })
  createdAt: Date;

  @Column()
  name: string;

  @ManyToMany(() => Audio, { cascade: true })
  @JoinTable()
  audios: Audio[];
}
