import { IGetAudio } from "../../audio/dto/get-audio.dto";

export interface IGetPlaylist {
  name: string,
  audios: IGetAudio[]
}
