import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { UserModule } from './user/user.module';
import { StorageModule } from './storage/storage.module';
import { AudioModule } from './audio/audio.module';
import { MulterModule } from '@nestjs/platform-express';
import { ConfigModule } from '@nestjs/config';
import databaseConfig from './config/database-config';
import jwtConfig from './config/jwt-config';
import { DbConfigModule } from './db-config/db-config.module';
import { DbConfigService } from './db-config/db-config.service';
import s3Config from './config/s3-config';
import { PlaylistModule } from './playlist/playlist.module';

@Module({
  imports: [
    AuthModule,
    UserModule,
    ConfigModule.forRoot({
      cache: true,
      isGlobal: true,
      load: [databaseConfig, jwtConfig, s3Config],
    }),
    TypeOrmModule.forRootAsync({
      imports: [DbConfigModule],
      useExisting: DbConfigService,
    }),
    MulterModule.register({
      dest: './files',
    }),
    StorageModule,
    AudioModule,
    PlaylistModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
