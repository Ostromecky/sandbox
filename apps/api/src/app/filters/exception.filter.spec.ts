import { GlobalExceptionFilter } from './global-exception.filter';

describe('ExceptionFilter', () => {
  it('should be defined', () => {
    expect(new GlobalExceptionFilter()).toBeDefined();
  });
});
