export class PageDto<T> {
  data: T[];
  count: number;

  constructor(dataSrc?: PageDto<T>) {
    if (dataSrc) {
      this.data = dataSrc.data;
      this.count = dataSrc.count;
    }
  }
}
