import { Global, Module } from '@nestjs/common';
import { StorageService } from './storage/storage.service';
import * as AWS from 'aws-sdk';
import { ConfigService } from '@nestjs/config';
import { S3Config } from '../config/s3-config';
import { S3_PROVIDER } from "./index";

const createS3ConnectionFactory = (configService: ConfigService): AWS.S3 => {
  const {
    useMinio,
    accessKeyId,
    secretAccessKey,
    region,
    url,
    signatureVersion,
    s3ForcePathStyle,
    sslEnabled,
  } = configService.get<S3Config>('s3');

  return new AWS.S3({
    accessKeyId,
    secretAccessKey,
    region,
    ...(useMinio
      ? {
          s3ForcePathStyle,
          sslEnabled,
          signatureVersion,
          endpoint: new AWS.Endpoint(url).href,
        }
      : {}),
  });
};

@Global()
@Module({
  providers: [
    StorageService,
    {
      provide: S3_PROVIDER,
      useFactory: createS3ConnectionFactory,
      inject: [ConfigService],
    },
  ],
  exports: [StorageService, S3_PROVIDER],
})
export class StorageModule {}
