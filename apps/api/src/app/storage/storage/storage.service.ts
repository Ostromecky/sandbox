import { Inject, Injectable, OnModuleInit } from "@nestjs/common";
import { IStorageService } from "../interfaces/storage-service";
import { AWSError, S3 } from "aws-sdk";
import { ConfigService } from "@nestjs/config";
import { S3Config } from "../../config/s3-config";

@Injectable()
export class StorageService implements IStorageService, OnModuleInit {
  async onModuleInit() {
    const s3Config = this.configService.get<S3Config>("s3");
    await this.createPublicBucketIfNotExists(s3Config.storageBucketName);
  }

  constructor(
    @Inject("S3") private s3: S3,
    private configService: ConfigService
  ) {
    s3.getBucketLocation();
  }

  async createPublicBucketIfNotExists(bucket: string): Promise<void> {
    const policyID = `${bucket}-public-get-policy`;
    const policy = `{
      "Version": "2012-10-17",
      "Statement": [
        {
          "Sid": "${policyID}",
          "Effect": "Allow",
          "Principal": {"AWS": "*"},
          "Action": [ "s3:GetObject", "s3:PutObject" ],
          "Resource": ["arn:aws:s3:::${bucket}/*" ]
        }
      ]
    }`;

    const bucketExists = await (async () => {
      try {
        await this.s3.headBucket({ Bucket: bucket }).promise();
        return true;
      } catch (e) {
        const error = e as AWSError;
        if (error.code === "NotFound") {
          return false;
        }
        throw e;
      }
    })();

    if (!bucketExists) {
      await this.s3.createBucket({ Bucket: bucket }).promise();
    }

    // Set CORS
    try {
      await this.s3
        .putBucketCors({
          Bucket: bucket,
          CORSConfiguration: {
            CORSRules: [
              {
                AllowedHeaders: ["*"],
                ExposeHeaders: ["ETag", "x-amz-meta-custom-header"],
                AllowedMethods: ["HEAD", "GET", "PUT", "POST", "DELETE"],
                AllowedOrigins: ["*"]
              }
            ]
          }
        })
        .promise();
    } catch (e) {
      // MinIO (used for local env) doest not support this, CORS is * by default
      const error = e as AWSError;
      if (error.code !== "NotImplemented") {
        throw error;
      }
    }

    // Set public read
    await this.s3
      .putBucketPolicy({
        Bucket: bucket,
        Policy: policy
      })
      .promise();
  }
}
