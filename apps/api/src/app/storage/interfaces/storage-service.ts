export interface IStorageService {
  createPublicBucketIfNotExists(bucket: string): Promise<void>;
}
