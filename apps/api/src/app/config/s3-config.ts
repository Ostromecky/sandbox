import { registerAs } from '@nestjs/config';

export interface S3Config {
  useMinio: boolean;
  accessKeyId: string;
  secretAccessKey: string;
  region: string;
  url: string;
  signatureVersion: string;
  s3ForcePathStyle: boolean;
  sslEnabled: boolean;
  storageBucketName: string;
}

export default registerAs<S3Config>(
  's3',
  (): S3Config => ({
    useMinio: JSON.parse(process.env.USE_MINIO) || false,
    accessKeyId: process.env.AWS_ACCESS_KEY_ID || 'key',
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY || 'secret1337',
    region: process.env.STORAGE_REGION || 'eu-west-1',
    url: process.env.STORAGE_URL || 'http://127.0.0.1:9000',
    signatureVersion: 'v4',
    s3ForcePathStyle: true,
    sslEnabled: false,
    storageBucketName: process.env.STORAGE_BUCKET_NAME || 'sample-bucket'
  }),
);
