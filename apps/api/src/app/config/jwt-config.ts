import { JwtModuleOptions } from '@nestjs/jwt/dist/interfaces/jwt-module-options.interface';
import { registerAs } from '@nestjs/config';

export interface JwtConfig {
  jwt: JwtModuleOptions;
  authEnabled: boolean;
}

export default registerAs<JwtConfig>('jwt', () => ({
  jwt: { secret: process.env.JWT_SECRET || 'secret' },
  authEnabled: JSON.parse(process.env.AUTH_ENABLED) || false,
}));
