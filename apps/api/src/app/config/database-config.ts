import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { registerAs } from '@nestjs/config';

export default registerAs<TypeOrmModuleOptions>(
  'database',
  (): TypeOrmModuleOptions => ({
    type: 'postgres' as const,
    host: process.env.DB_HOST || 'localhost',
    port: Number(process.env.DB_PORT) || 5432,
    username: process.env.DB_USER || 'postgres',
    password: process.env.DB_PASSWORD || 'postgres',
    database: process.env.DB_SCHEMA || 'sandbox',
    autoLoadEntities: true,
    ssl: JSON.parse(process.env.DB_SSL),
    // extra: { ssl: { rejectUnauthorized: false } },
    synchronize: JSON.parse(process.env.DB_SYNC) || false
  }),
);
