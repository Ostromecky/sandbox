import { Injectable } from '@nestjs/common';
import { TypeOrmModuleOptions, TypeOrmOptionsFactory } from '@nestjs/typeorm';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class DbConfigService implements TypeOrmOptionsFactory {
  private readonly databaseConfig: TypeOrmModuleOptions;

  constructor(private configService: ConfigService) {
    this.databaseConfig = this.configService.get<TypeOrmModuleOptions>(
      'database',
    );
  }

  createTypeOrmOptions(): TypeOrmModuleOptions {
    return this.databaseConfig;
  }
}
