import { Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';

export const DASHBOARD_ROUES: Routes = [
  {
    path: '',
    component: DashboardComponent,
  },
];
