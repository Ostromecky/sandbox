import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { DASHBOARD_ROUES } from './dashboard.routes';
import { DashboardComponent } from './dashboard/dashboard.component';

const DASHBOARD_DECLARATIONS = [DashboardComponent];

@NgModule({
  declarations: [DASHBOARD_DECLARATIONS],
  imports: [SharedModule, RouterModule.forChild(DASHBOARD_ROUES)],
})
export class DashboardModule {}
