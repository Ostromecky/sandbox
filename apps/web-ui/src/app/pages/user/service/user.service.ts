import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserQuery } from '../model/user-query.model';
import { Page } from '../../../shared/model/page.model';
import { IUser } from '../model/user.model';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private http: HttpClient) {}

  fetchUsers(query: UserQuery): Observable<Page<IUser>> {
    const params = this.toParams(query);
    return this.http.get<Page<IUser>>('/api/users', { params });
  }

  private toParams(filters: { [key: string]: any } = {}): HttpParams {
    let params = new HttpParams();
    for (const key of Object.keys(filters)) {
      const value = filters[key].toString().trim();
      params = value ? params.append(key, value) : params;
    }
    return params;
  }
}
