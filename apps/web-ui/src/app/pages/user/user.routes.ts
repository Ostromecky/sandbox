import { Routes } from '@angular/router';
import { UsersComponent } from './users/users.component';

export const USER_ROUTES: Routes = [
  {
    path: '',
    component: UsersComponent,
  },
];
