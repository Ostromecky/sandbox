export class UserQuery {
  firstName: string;
  lastName: string;
  isActive: boolean;
  take: number;
  skip: number;

  constructor(query?: UserQuery) {
    if (query) {
      Object.assign(this, query);
    }
  }
}
