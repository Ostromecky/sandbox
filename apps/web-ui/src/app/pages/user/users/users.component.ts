import { Component } from '@angular/core';
import { UserService } from '../service/user.service';
import { UserQuery } from '../model/user-query.model';
import { IUser } from '../model/user.model';

@Component({
  selector: 'forpy-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent {
  data: IUser[];
  displayedColumns: string[] = ['firstName', 'lastName', 'isActive', 'actions'];
  constructor(private userService: UserService) {}

  ngOnInit(): void {
    this.userService
      .fetchUsers(new UserQuery())
      .subscribe((res) => (this.data = res.data));
  }
}
