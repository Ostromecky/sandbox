import { Routes } from '@angular/router';
import { PlaylistPageComponent } from './playlist-page/playlist-page.component';

export const ROUTES: Routes = [
  {
    path: '',
    component: PlaylistPageComponent,
  }
];
