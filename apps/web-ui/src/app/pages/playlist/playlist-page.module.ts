import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ROUTES } from './playlist.routes';
import { PlaylistModule } from '../../features/playlist/playlist.module';
import { PlaylistPageComponent } from './playlist-page/playlist-page.component';
import { UploadModule } from "../../features/upload/upload.module";

@NgModule({
  declarations: [PlaylistPageComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(ROUTES),
    PlaylistModule,
    UploadModule
  ],
})
export class PlaylistPageModule {}
