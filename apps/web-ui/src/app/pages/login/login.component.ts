import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { AuthenticationService } from '../../core/services/authentication.service';
import { Store } from '@ngrx/store';
import * as LoginActions from './store/login.actions';
import { selectLoginLoading } from './store/login.reducer';

@Component({
  selector: 'forpy-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginComponent implements OnInit {
  constructor(
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    private store: Store,
  ) {}

  loginForm: FormGroup;
  loading$ = this.store.select(selectLoginLoading);
  error = '';

  ngOnInit(): void {
    this.initForm();
  }

  submit() {
    if (this.loginForm.invalid) {
      return;
    }

    this.store.dispatch(
      LoginActions.Login({
        login: this.login.value,
        password: this.password.value,
      }),
    );
  }

  backToPreviousPage() {
    history.back();
  }

  private initForm() {
    this.loginForm = this.formBuilder.group({
      login: ['', [Validators.required]],
      password: ['', Validators.required],
    });
  }

  get password(): AbstractControl {
    return this.loginForm.get('password');
  }

  get login(): AbstractControl {
    return this.loginForm.get('login');
  }
}
