import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { LoginComponent } from './login.component';
import { LOGIN_ROUTES } from './login.routes';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { LoginEffects } from './store/login.effects';
import * as loginFeature from './store/login.reducer';

@NgModule({
  imports: [
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    SharedModule,
    MatIconModule,
    RouterModule.forChild(LOGIN_ROUTES),
    StoreModule.forFeature(
      loginFeature.loginFeatureKey,
      loginFeature.loginReducer,
    ),
    EffectsModule.forFeature([LoginEffects]),
  ],
  declarations: [LoginComponent],
})
export class LoginModule {}
