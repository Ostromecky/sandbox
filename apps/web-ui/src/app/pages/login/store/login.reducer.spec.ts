import * as fromReducer from './login.reducer';
import { Login, LoginSuccess } from './login.actions';
import { ILoginState } from './login.reducer';

describe('LoginReducer', () => {
  describe('unknown action', () => {
    it('should return the default state', () => {
      const { loginInitialState } = fromReducer;
      const action = {
        type: 'Unknown',
      };
      const state = fromReducer.loginReducer(loginInitialState, action);

      expect(state).toBe(loginInitialState);
    });
  });

  describe('retrievedBookList action', () => {
    it('should retrieve all books and update the state in an immutable way', () => {
      const { loginInitialState } = fromReducer;
      const newState: ILoginState = {
        loggedIn: false,
        loading: true,
      };
      const action = Login({ login: 'user', password: 'password' });
      const state = fromReducer.loginReducer(loginInitialState, action);

      expect(state).toEqual(newState);
      expect(state).not.toBe(newState);
    });
  });

  describe('Process login success', () => {
    it('Should execute login success action', () => {
      const { loginInitialState } = fromReducer;
      const newState: ILoginState = {
        loggedIn: true,
        loading: false,
      };
      const action = LoginSuccess();
      const state = fromReducer.loginReducer(loginInitialState, action);

      expect(state).toEqual(newState);
      expect(state).not.toBe(newState);
    });
  });
});
