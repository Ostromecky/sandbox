import { createAction, props } from '@ngrx/store';

export const Login = createAction(
  '[Login] Login Process',
  props<{
    login: string;
    password: string;
  }>(),
);

export const LoginSuccess = createAction('[Login] Login Success');
export const LoginError = createAction('[Login] Login Error');
