import {
  createFeatureSelector,
  createReducer,
  createSelector,
  on,
} from '@ngrx/store';
import * as LoginActions from './login.actions';
import { IAppState } from '../../../app.reducer';

export interface ILoginState {
  loggedIn: boolean;
  loading: boolean;
}

export const loginInitialState: ILoginState = {
  loggedIn: false,
  loading: false,
};

export const loginFeatureKey = 'login';

export const loginReducer = createReducer<ILoginState>(
  loginInitialState,
  on(LoginActions.Login, (state: ILoginState) => ({ ...state, loading: true })),
  on(LoginActions.LoginSuccess, () => ({
    loggedIn: true,
    loading: false,
  })),
  on(LoginActions.LoginError, () => ({
    loggedIn: false,
    loading: false,
  })),
);

export const getLoginState = createFeatureSelector<IAppState, ILoginState>(
  loginFeatureKey,
);

export const selectLoginLoading = createSelector(
  getLoginState,
  (state: ILoginState) => state.loading,
);
