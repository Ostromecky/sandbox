import { provideMockActions } from '@ngrx/effects/testing';
import { Observable, of } from 'rxjs';
import * as LoginActions from './login.actions';
import { LoginEffects } from './login.effects';
import { AuthenticationService } from '../../../core/services/authentication.service';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { Actions } from '@ngrx/effects';
import { TestBed } from '@angular/core/testing';
import { cold, hot } from 'jest-marbles';

const initialState = {
  loading: false,
  loggedIn: true,
};

describe('LoginEffects', () => {
  let actions$: Observable<any>;
  let effects: LoginEffects;
  let store: MockStore<any>;
  let authenticationService: AuthenticationService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        LoginEffects,
        provideMockActions(() => actions$),
        provideMockStore({ initialState }),
        {
          provide: AuthenticationService,
          useValue: {
            login: jest.fn(),
          },
        },
      ],
    });
    effects = TestBed.inject<LoginEffects>(LoginEffects);
    store = TestBed.inject(MockStore);
    actions$ = TestBed.inject(Actions);
    authenticationService = TestBed.inject(AuthenticationService);
  });
  //
  describe('login effect via subscription', () => {
    test('123', (done) => {
      actions$ = of(LoginActions.Login({ login: '', password: '' }));
      authenticationService.login = jest
        .fn()
        .mockImplementationOnce(() => of(true));
      effects.login$.subscribe((res) => {
        expect(res).toEqual(LoginActions.LoginSuccess());
        expect(authenticationService.login).toHaveBeenCalledTimes(1);
        done();
      });
    });
  });

  describe('login effect via marbles', () => {
    test('123', () => {
      actions$ = hot('a', {
        a: LoginActions.Login({ login: '', password: '' }),
      });
      authenticationService.login = jest.fn(() => cold('-a|', { a: true }));
      const expected = hot('-b', { b: LoginActions.LoginSuccess() });
      expect(effects.login$).toBeObservable(expected);
    });
  });

  describe('login error effect via marbles', () => {
    test('123', () => {
      actions$ = hot('-a', {
        a: LoginActions.Login({ login: '', password: '' }),
      });
      authenticationService.login = jest.fn(() =>
        cold('-#|', { a: new Error() }),
      );
      const expected = hot('--b', { b: LoginActions.LoginError() });
      expect(effects.login$).toBeObservable(expected);
    });
  });
});
