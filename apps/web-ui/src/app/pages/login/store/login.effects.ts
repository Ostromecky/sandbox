import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { AuthenticationService } from '../../../core/services/authentication.service';
import * as LoginActions from './login.actions';
import { of } from 'rxjs';

@Injectable()
export class LoginEffects {
  login$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LoginActions.Login),
      mergeMap((action) =>
        this.authService.login(action.login, action.password).pipe(
          map(() => LoginActions.LoginSuccess()),
          catchError(() => of(LoginActions.LoginError())),
        ),
      ),
    ),
  );

  constructor(
    private actions$: Actions,
    private authService: AuthenticationService,
  ) {}
}
