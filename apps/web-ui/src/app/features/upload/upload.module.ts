import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import * as fromUpload from './store/reducers/upload.reducer';
import { EffectsModule } from '@ngrx/effects';
import { UploadEffects } from './store/effects/upload.effects';
import { UploadComponent } from './components/upload/upload.component';
import { MatButtonModule } from "@angular/material/button";
import { MatIconModule } from "@angular/material/icon";
import { HttpClientModule } from "@angular/common/http";
import { UploadService } from "./services/upload.service";

@NgModule({
  declarations: [UploadComponent],
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
    HttpClientModule,
    StoreModule.forFeature(fromUpload.uploadFeatureKey, fromUpload.reducer),
    EffectsModule.forFeature([UploadEffects])
  ],
  exports: [
    UploadComponent
  ],
  providers: [UploadService]
})
export class UploadModule { }
