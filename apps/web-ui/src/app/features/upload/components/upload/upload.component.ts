import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromUpload from '../../store/actions/upload.actions';

@Component({
  selector: 'forpy-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UploadComponent implements OnInit {
  constructor(private store$: Store) {}

  ngOnInit(): void {}

  audioChange(event: Event) {
    const target = event.target as HTMLInputElement;
    const files = target.files as FileList;
    const file = files[0] as File;
    const formData = this.fileToFormData(file);
    this.store$.dispatch(fromUpload.addFile({ formData }));
  }

  fileToFormData(file: File): FormData {
    const formData = new FormData();
    formData.append('file', file, file.name);
    return formData;
  }
}
