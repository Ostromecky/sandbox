import { createFeatureSelector } from '@ngrx/store';
import * as fromUpload from '../reducers/upload.reducer';

export const selectUploadState = createFeatureSelector<fromUpload.State>(
  fromUpload.uploadFeatureKey
);
