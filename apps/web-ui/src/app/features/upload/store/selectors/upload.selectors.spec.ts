import * as fromUpload from '../reducers/upload.reducer';
import { selectUploadState } from './upload.selectors';

describe('Upload Selectors', () => {
  it('should select the feature state', () => {
    const result = selectUploadState({
      [fromUpload.uploadFeatureKey]: {}
    });

    expect(result).toEqual({});
  });
});
