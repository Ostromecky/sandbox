import * as fromUpload from './upload.actions';

describe('uploadUploads', () => {
  it('should return an action', () => {
    expect(fromUpload.addFile().type).toBe('[Upload] Upload Uploads');
  });
});
