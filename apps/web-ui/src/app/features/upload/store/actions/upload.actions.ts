import { createAction, props } from '@ngrx/store';

export const addFile = createAction(
  '[Upload] Add Audio File',
  props<{ formData: FormData }>(),
);

export const addFileSuccess = createAction('[Upload] Add File Success');

export const addFileFailure = createAction(
  '[Upload] Add File Failure',
  props<{ error: any }>(),
);

export const requestSignedUploadUrl = createAction(
  '[Upload] Request Signed Upload Url'
);

export const requestSignedUploadUrlSuccess = createAction(
  '[Upload] Request Signed Upload Url Success'
);

export const requestSignedUploadUrlFailure = createAction(
  '[Upload] Request Signed Upload Url Failure'
);



