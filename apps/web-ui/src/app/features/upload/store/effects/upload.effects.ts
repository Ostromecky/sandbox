import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, concatMap, map } from 'rxjs/operators';
import { of } from 'rxjs';

import * as UploadActions from '../actions/upload.actions';
import { UploadService } from '../../services/upload.service';

@Injectable()
export class UploadEffects {
  addFile$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UploadActions.addFile),
      concatMap((action) =>
        this.audioService.addAudioFile(action.formData).pipe(
          map((data) => UploadActions.addFileSuccess()),
          catchError((error) => of(UploadActions.addFileFailure({ error }))),
        ),
      ),
    );
  });

  constructor(private actions$: Actions, private audioService: UploadService) {}
}
