import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { UploadEffects } from './upload.effects';

describe('UploadEffects', () => {
  let actions$: Observable<any>;
  let effects: UploadEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        UploadEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.inject(UploadEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
