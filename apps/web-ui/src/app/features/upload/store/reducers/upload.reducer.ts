import { createReducer, on } from '@ngrx/store';
import * as UploadActions from '../actions/upload.actions';

export const uploadFeatureKey = 'upload';

export interface State {

}

export const initialState: State = {

};

export const reducer = createReducer(
  initialState,
  on(UploadActions.addFile, state => state),
  on(UploadActions.addFileSuccess, (state, action) => state),
  on(UploadActions.addFileFailure, (state, action) => state),
);
