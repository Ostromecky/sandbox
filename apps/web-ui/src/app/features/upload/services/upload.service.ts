import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class UploadService {
  constructor(private http: HttpClient) {}

  addAudioFile(formData: FormData): Observable<any> {
    return this.http.post<FormData>('/api/audio', formData);
  }

  requestSignedUrl(fileType: string): Observable<{ uploadURL: string }> {
    const params = { fileType };
    return this.http.get<{ uploadURL: string }>('/api/audio/url', { params });
  }
}
