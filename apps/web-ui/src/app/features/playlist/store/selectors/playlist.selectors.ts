import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromPlaylist from '../reducers/playlist.reducer';
import { PlaylistState } from "../reducers/playlist.reducer";

export const selectPlaylistState = createFeatureSelector<fromPlaylist.PlaylistState>(
  fromPlaylist.playlistFeatureKey
);

export const selectPlaylist = createSelector(
  selectPlaylistState,
  (state: PlaylistState) => state.audioFiles
)
