import * as fromAudio from './playlist.actions';

describe('loadPlaylist', () => {
  it('should return an action', () => {
    expect(fromAudio.loadAudios().type).toBe('[Audio] Load Audios');
  });
});
