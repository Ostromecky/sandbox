import { createAction, props } from '@ngrx/store';
import { AudioFile } from '../../../../core/services/api.service';

export const loadPlaylist = createAction('[Playlist] Load Playlist');

export const loadPlaylistSuccess = createAction(
  '[Playlist] Load Playlist Success',
  props<{ files: AudioFile[] }>(),
);

export const loadPlaylistFailure = createAction(
  '[Playlist] Load Playlist Failure',
  props<{ errors }>(),
);

export const selectFile = createAction('[Playlist] Select file');
