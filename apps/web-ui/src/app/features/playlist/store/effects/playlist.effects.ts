import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, concatMap, map } from 'rxjs/operators';
import { of } from 'rxjs';

import * as playlistActions from '../actions/playlist.actions';
import { AudioFile, ApiService } from '../../../../core/services/api.service';

@Injectable()
export class PlaylistEffects {
  loadAPlaylist$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(playlistActions.loadPlaylist),
      concatMap(() =>
        this.cloudService.getFiles().pipe(
          map((files: AudioFile[]) => playlistActions.loadPlaylistSuccess({ files })),
          catchError((error) => of(playlistActions.loadPlaylistFailure(error))),
        ),
      ),
    );
  });

  constructor(private actions$: Actions, private cloudService: ApiService) {}
}
