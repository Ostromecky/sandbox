import { createReducer, on } from '@ngrx/store';
import * as AudioActions from '../actions/playlist.actions';
import { AudioFile } from '../../../../core/services/api.service';

export const playlistFeatureKey = 'playlist';

export interface PlaylistState {
  loading: boolean;
  audioFiles: AudioFile[];
  // playing: boolean;
  // readableCurrentTime: string;
  // readableDuration: string;
  // duration: number;
  // currentTime: number;
  // canplay: boolean;
  // error: boolean;
}

export const initialState: PlaylistState = {
  loading: false,
  audioFiles: [],
  // playing: false,
  // readableCurrentTime: '',
  // readableDuration: '',
  // duration: undefined,
  // currentTime: undefined,
  // canplay: false,
  // error: false,
};

export const playlistReducer = createReducer(
  initialState,

  on(AudioActions.loadPlaylist, (state) => ({
    ...state,
    loading: true,
  })),
  on(AudioActions.loadPlaylistSuccess, (state, { files }) => ({
    ...state,
    loading: false,
    audioFiles: files,
  })),
  on(AudioActions.loadPlaylistFailure, (state) => ({
    ...state,
    loading: false,
  })),
);
