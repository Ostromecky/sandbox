export enum AudioEvent {
  ENDED = 'ended',
  ERROR = 'error',
  PLAY = 'play',
  PLAYING = 'playing',
  PAUSE = 'pause',
  TIMEUPDATE = 'timeupdate',
  CANPLAY = 'canplay',
  LOADEDMETADATA = 'loadedmetadata',
  LOADSTART = 'loadstart',
}
