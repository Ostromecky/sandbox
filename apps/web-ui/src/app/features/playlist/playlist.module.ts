import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import * as fromAudio from './store/reducers/playlist.reducer';
import { EffectsModule } from '@ngrx/effects';
import { PlaylistEffects } from './store/effects/playlist.effects';
import { PlaylistComponent } from './components/playlist/playlist.component';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [PlaylistComponent],
  exports: [PlaylistComponent],
  imports: [
    CommonModule,
    MatListModule,
    MatIconModule,
    StoreModule.forFeature(
      fromAudio.playlistFeatureKey,
      fromAudio.playlistReducer,
    ),
    EffectsModule.forFeature([PlaylistEffects]),
  ],
})
export class PlaylistModule {}
