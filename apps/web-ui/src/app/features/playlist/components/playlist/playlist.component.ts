import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AudioStateService } from '../../../../core/services/audio-state.service';
import { AudioService } from '../../../../core/services/audio.service';
import { Store } from '@ngrx/store';
import { selectPlaylist } from '../../store/selectors/playlist.selectors';
import { Observable } from 'rxjs';
import * as playlistActions from '../../store/actions/playlist.actions';
import { AudioFile } from "../../../../core/services/api.service";

@Component({
  selector: 'forpy-playlist',
  templateUrl: './playlist.component.html',
  styleUrls: ['./playlist.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PlaylistComponent {
  currentFile: any = {};
  playlist$: Observable<AudioFile[]>;

  constructor(
    public audioStateService: AudioStateService,
    public audioService: AudioService,
    private store: Store,
  ) {
    this.playlist$ = store.select(selectPlaylist);
    this.store.dispatch(playlistActions.loadPlaylist());
  }

  openFile(file, index) {
    this.audioStateService.currentFile = { index, file };
    this.audioStateService.stop();
    this.audioStateService.playStream(file.url);
  }
}
