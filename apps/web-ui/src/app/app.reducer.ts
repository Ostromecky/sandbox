import { ActionReducerMap } from '@ngrx/store';
import { ILoginState, loginReducer } from './pages/login/store/login.reducer';
export interface IAppState {
  login: ILoginState;
}

export const reducers: ActionReducerMap<IAppState> = {
  login: loginReducer,
};
