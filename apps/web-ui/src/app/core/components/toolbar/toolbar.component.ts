import { Component } from '@angular/core';

@Component({
  selector: 'forpy-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent {}
