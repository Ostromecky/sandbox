import { Component } from '@angular/core';
import { AudioService } from '../../services/audio.service';
import { AudioStateService } from '../../services/audio-state.service';

@Component({
  selector: 'forpy-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss'],
})
export class PlayerComponent {
  constructor(
    public audioStateService: AudioStateService,
    public audioService: AudioService,
  ) {}

  pause() {
    this.audioStateService.pause();
  }

  play() {
    this.audioStateService.play();
  }

  stop() {
    this.audioStateService.stop();
  }

  next() {
    const index = this.audioStateService.currentFile.index + 1;
    const file = this.audioStateService.files[index];
    this.audioStateService.openFile(file, index);
  }

  previous() {
    const index = this.audioStateService.currentFile.index - 1;
    const file = this.audioStateService.files[index];
    this.audioStateService.openFile(file, index);
  }

  isFirstPlaying() {
    return this.audioStateService.currentFile.index === 0;
  }

  isLastPlaying() {
    return (
      this.audioStateService.currentFile.index ===
      this.audioStateService.files.length - 1
    );
  }

  onSliderChangeEnd(change) {
    this.audioStateService.onSliderChangeEnd(change.value);
  }
}
