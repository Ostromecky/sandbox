import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";

export interface AudioFile {
  url: string;
  name: string;
  artist: string;
  imgUrl?: string;
}

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  // files: AudioFile[] = [
  //   // tslint:disable-next-line: max-line-legth
  //   {
  //     url:
  //       'https://ia801504.us.archive.org/3/items/EdSheeranPerfectOfficialMusicVideoListenVid.com/Ed_Sheeran_-_Perfect_Official_Music_Video%5BListenVid.com%5D.mp3',
  //     name: 'Perfect',
  //     artist: ' Ed Sheeran',
  //   },
  //   {
  //     url:
  //       '/assets/TacoHemingwayEuropa/09TacoHemingwayWWANIEBerlin.mp3',
  //     name: 'WWA NIE Berlin',
  //     artist: ' Taco Hemingway',
  //   },
  //   {
  //     url: '/assets/TacoHemingwayEuropa/05-TacoHemingway-NieUfamSobieSam.mp3',
  //     name: 'Nie Ufam Sobie Sam',
  //     artist: 'Taco Hemingway',
  //     imgUrl: '/assets/TacoHemingwayEuropa/cover.jpg'
  //   },
  // ];

  constructor(
    private http: HttpClient,
  ) {}

  getFiles(): Observable<AudioFile[]> {
    return this.http.get<AudioFile[]>('/api/audio');
  }
}
