import { Injectable } from '@angular/core';
import { AudioService } from './audio.service';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root',
})
export class AudioStateService {
  files: Array<any> = [];
  currentFile: any = {};

  constructor(
    private audioService: AudioService,
    private cloudService: ApiService,
  ) {
    cloudService.getFiles().subscribe((files) => (this.files = files));
  }

  openFile(file, index) {
    this.currentFile = { index, file };
    this.audioService.stop();
    this.playStream(file.url);
  }

  onSliderChangeEnd(change) {
    this.audioService.seekTo(change);
  }

  pause() {
    this.audioService.pause();
  }

  play() {
    this.audioService.play();
  }

  stop() {
    this.audioService.stop();
  }

  playStream(url: string) {
    this.audioService.playStream(url).subscribe((events) => {
      // listening for fun here
    });
  }
}
