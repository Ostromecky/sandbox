import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { User } from '../models/user.model';
import { CookieService } from 'ngx-cookie-service';
import { CreateSession } from '@sandbox/types';
import { catchError, map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
  private userSubject: BehaviorSubject<User>;
  public user: Observable<User>;

  loggedIn = new BehaviorSubject<boolean>(false);
  role = new BehaviorSubject<number[]>([0]);

  constructor(private http: HttpClient, private cookieService: CookieService) {}

  login(login: string, password: string) {
    const credentials = new CreateSession({
      userName: login,
      password: password,
    });
    return this.http.post<CreateSession>('/api/auth', credentials);
  }

  logout() {
    const headers = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
      withCredentials: true,
    };
    return this.http
      .get<any>('/api/users/logout', headers)
      .subscribe(() => this.userStatus());
  }

  userStatus() {
    this.checkLogin();
    this.checkRole();
  }

  checkLogin() {
    return this.http
      .post<any>('/api/users/cookie', {}, { withCredentials: true })
      .subscribe((isLoggedIn: boolean) => this.loggedIn.next(isLoggedIn));
  }

  checkRole() {
    return this.http
      .post<any>('/api/users/role', {}, { withCredentials: true })
      .subscribe((role: number[]) => {
        this.role.next(role);
      });
  }

  register(payload, httpOptions) {
    return this.http.post<any>('/api/user/register', payload, httpOptions).pipe(
      map((e) => e),
      catchError((e) => throwError(e)),
    );
  }

  activate(activationCode: string): Observable<boolean> {
    /// TODO activate_code swap to one_time_code
    return this.http
      .post<boolean>('/api/user/activate', {
        activationCode: activationCode,
      })
      .pipe(
        map((cookie) => cookie),
        catchError((e) => throwError(e)),
      );
  }

  reset(email?, login?) {
    return this.http.post<string>('/api/user/reset', email || login).pipe(
      map((status) => status),
      catchError((e) => throwError(e)),
    );
  }

  updatePasswordAfterReset(payload) {
    /// TODO reset swap to one_time_code
    return this.http.post<string>('/api/user/updatePassword', payload).pipe(
      map((status) => status),
      catchError((e) => throwError(e)),
    );
  }
}
