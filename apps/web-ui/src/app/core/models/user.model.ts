export class User {
  id: number;
  login: string;
  password: string;
  name: string;
  surname: string;
  email: string;
  phone: string;
  status: number; // 0 inactive, 1 active, 2 blocked, 3 deleted
  role: string; // TODO: tymczasowo do usunięcia
  token: string;
}
