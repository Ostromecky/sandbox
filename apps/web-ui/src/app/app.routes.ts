import { Routes } from "@angular/router";
import { HomeComponent } from "./core/components/home/home.component";

export const APP_ROUTES: Routes = [
  {
    path: "login",
    loadChildren: () =>
      import("./pages/login/login.module").then((m) => m.LoginModule)
  },
  {
    path: "",
    // canActivate: [AuthGuard],
    // canActivateChild: [AuthGuard],
    component: HomeComponent,
    children: [
      {
        path: "dashboard",
        loadChildren: () =>
          import("./pages/dashboard/dashboard.module").then(
            (m) => m.DashboardModule
          )
        // canLoad: [AuthGuard],
      },
      {
        path: "users",
        loadChildren: () =>
          import("./pages/user/user.module").then((m) => m.UserModule)
      },
      {
        path: "playlist/:id",
        loadChildren: () =>
          import("./pages/playlist/playlist-page.module").then((m) => m.PlaylistPageModule)
      }
    ]
  },
  { path: "**", redirectTo: "dashboard", pathMatch: "full" }
];
