export class Page<T> {
  data: T[];
  count: number;

  constructor(dataSrc?: Page<T>) {
    if (dataSrc) {
      this.data = dataSrc.data;
      this.count = dataSrc.count;
    }
  }
}
