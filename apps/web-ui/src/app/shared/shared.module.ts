import { FlexLayoutModule } from '@angular/flex-layout';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutServerModule } from '@angular/flex-layout/server';

const SHARED_IMPORTS = [
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
  FlexLayoutModule,
  FlexLayoutServerModule,
];

@NgModule({
  imports: [SHARED_IMPORTS],
  declarations: [],
  exports: [SHARED_IMPORTS],
})
export class SharedModule {}
