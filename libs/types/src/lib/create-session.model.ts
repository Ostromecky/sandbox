export class CreateSession {
  userName: string;
  password: string;

  constructor(args: CreateSession) {
    Object.assign(this, args);
  }
}
