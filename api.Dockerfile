FROM node:12.16.3-alpine

WORKDIR /usr/src/app

COPY dist/apps/api dist

COPY . .

RUN npm install --only=prod

EXPOSE 3000

CMD ["node", "dist/main.js"]
